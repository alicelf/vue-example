import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import ShoppingList from './views/ShoppingList.vue';
import Checkout from './views/Checkout.vue';
import PayNow from './views/PayNow.vue';
import ThankYou from './views/ThankYou.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/shopping-list',
      name: 'shoppinglist',
      component: ShoppingList,
    },
    {
      path: '/checkout',
      name: 'checkout',
      component: Checkout,
    },
    {
      path: '/pay-now',
      name: 'paynow',
      component: PayNow,
    },
    {
      path: '/thank-you',
      name: 'thankyou',
      component: ThankYou,
    },
  ],
});
