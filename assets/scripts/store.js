import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// configure central storage
// more modules goes there
export default new Vuex.Store({
  modules: {
    products : require('./modules/products'),
	},
});
