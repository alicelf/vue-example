const state = {
  products: [],
  tmpProducts: [],
  tmpProductsArrived: false,
}

const mutations = {
	setProducts(state, data) {
		state.products = data
  },
  removeFromCart(state, sku) {
    state.products = state.products.filter((item) => item.sku !== sku )
  },

  setTmpProducts(state, data) {
    state.tmpProducts = data;
    state.tmpProductsArrived = true;
  },
  // approve
  approve(state) {
    state.products = [...state.tmpProducts].map((item) => {
      item.isNew = false;
      return item;
    });

    state.tmpProducts = [];
    state.tmpProductsArrived = false;
  },
  // decline
  decline(state) {
    state.tmpProducts = [];
    state.tmpProductsArrived = false;
  }
};

const actions = {
  // dispatch, commit, state
  removeFromCart({commit}, sku) {
    return new Promise((resolve, reject) => {
      const success = true;
      commit('removeFromCart', sku);
      success ? resolve() : reject();
    })
  },

  increment({ commit, state }, product) {
    return new Promise((resolve, reject) => {
      const success = true;
      let cardItems = [...state.products];
      const found = cardItems.findIndex((item) => item.sku === product.sku)
      if(found > -1) {
        cardItems[found].qty ++;
        commit('setProducts', cardItems);
      }
      success ? resolve() : reject();
    });
  },

  decrement({ commit, state }, product) {
    return new Promise((resolve, reject) => {
      const success = true;
      let cardItems = [...state.products];
      const found = cardItems.findIndex((item) => item.sku === product.sku)
      if(found > -1 ) {
        cardItems[found].qty--;
        commit('setProducts', cardItems);
      }
      success ? resolve() : reject();
    });
  },
}

module.exports = {
	state,
  mutations,
  actions
};