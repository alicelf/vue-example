const CommonComputed = {
  computed: {
    newProductsArrived() {
      return this.$store.state.products.tmpProductsArrived;
    },
    getProducts() {
      return this.$store.state.products.products;
    },
    getTmpProducts() {
      return this.$store.state.products.tmpProducts;
    },
    getSubtotal() {
      const operableArray = this.getTmpProducts.length > 0 ?
        this.getTmpProducts : this.getProducts;
  
      let tmp = operableArray.map((item) => item.price * item.qty)
      if(tmp.length === 0) {
        return 0
      }
      tmp =  tmp.reduce((accomulator, currentValue) => accomulator + currentValue);
  
      // @TEMPORARY: tax is hardcoded
      return (tmp + 30.2).toFixed(2)
    }
  }
}

export default CommonComputed;