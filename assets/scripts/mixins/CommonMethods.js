import Socket from "../socket";
import * as dt from '../../mocks/products.json';

const CommonMethods = {
  methods: {
    renderCurrentDate() {
      // @TODO: render actual date with new Date() e.t.c
      return "11 Jul 2018  13:45";
    },
    sendRequest() {
      // @TEMPORARY - Faked new items
      setTimeout(() => {
        let iterator = 0;
        const appended = [...dt.slice(2, 4)]
          .map((item) => {
            const itm = {...item};
              iterator ++;
              itm.sku = `${itm.sku}${iterator}`;
              itm.isNew = true;
              return itm;
          });
        const prepareNew = [
          ...appended,
          ...dt,
        ]
        Socket.send(JSON.stringify(prepareNew))
      }, 1000);
    },
    getSubtotalRaw(raw = false) {
      const operableArray = this.getTmpProducts.length > 0 ?
        this.getTmpProducts : this.getProducts;
  
      let tmp = operableArray.map((item) => item.price * item.qty)
      if(tmp.length === 0) {
        return 0
      }
      tmp =  tmp.reduce((accomulator, currentValue) => accomulator + currentValue);
  
      // @TEMPORARY: tax is hardcoded
      return raw ? tmp.toFixed(2) : (tmp + 30.2).toFixed(2)
    }
  }
}

export default CommonMethods;
